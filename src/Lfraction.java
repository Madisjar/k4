import java.util.*;

import static java.lang.Math.abs;
import static java.lang.Math.round;
import static java.lang.Math.pow;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b != 0) {
         long gcd = gcd(a, b);
         numerator = a / gcd;
         denominator = b / gcd;
      } else {
         throw new RuntimeException("Denominator b should not be 0. Fraction: " + a + "/" + b);
      } if (a == 0) {
         denominator = 1;
      }

   }

   /** Public method to access the numerator field.
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field.
    * @return denominator
    */
   public long getDenominator() {
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return numerator + "/" + denominator;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {

      try {
         Lfraction another = (Lfraction) m;
         return compareTo(another) == 0;
      } catch (Exception e) {
         throw new RuntimeException("Object is not Lfraction type: " + m.toString());
      }
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {

      return (int) ((getNumerator() + getDenominator()) * getDenominator());
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long newDenominator = m.getDenominator() * this.getDenominator();

      long newNumerator1 = getNumerator() * (newDenominator / getDenominator());
      long newNumerator2 = m.getNumerator() * (newDenominator / m.getDenominator());

      Lfraction f1 = new Lfraction(newNumerator1 + newNumerator2, newDenominator);

      return f1;
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long newNumerator = getNumerator() * m.getNumerator();
      long newDenominator = getDenominator() * m.getDenominator();
      return new Lfraction(newNumerator, newDenominator);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      long newNumerator = getDenominator();
      long newDenominator = getNumerator();
      if (newDenominator != 0) {

         if (newDenominator < 0) {
            newDenominator = -newDenominator;
            newNumerator = -newNumerator;
         }

      } else {
         throw new RuntimeException("Can't inverse fraction, because new denominator would be 0." + toString());
      }
      return new Lfraction(newNumerator, newDenominator);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-1 * getNumerator(), getDenominator());
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      return plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      try {

         return times(m.inverse());
      } catch (Exception e) {
         throw new RuntimeException("Can't divide fractions, because answer would have 0 as denominator: "
                 + toString()
                 + "divided by"
                 + m.toString());
      }
   }

   /** Multiplication of fractions.
    * @param power - power of int value
    * @return this*m
    */
   public Lfraction pow (int power) {
      boolean negativePower = false;
      if (power == 0) {
         return new Lfraction(1, 1);
      } else if (power < 0) {
         negativePower = true;
         power = -1 * power;
      }
      long newNumerator = getNumerator();
      long newDenominator = getDenominator();

      while (power > 1) {
         newNumerator = newNumerator * getNumerator();
         newDenominator = newDenominator * getDenominator();
         power--;
      }
      if (negativePower) {
         return new Lfraction(newNumerator, newDenominator).inverse();
      } else {
         return new Lfraction(newNumerator, newDenominator);
      }
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      if (toDouble() < m.toDouble()) {
         return -1;
      } else if (toString().equals(m.toString())) {
         return 0;
      }
      return 1;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(getNumerator(), getDenominator());
   }

   /** Integer part of the (improper) fraction.
    * @return integer part of this fraction
    */
   public long integerPart() {
      return (int) toDouble();
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      long leftOver = getNumerator() - integerPart() * getDenominator();
      return new Lfraction(leftOver, getDenominator());
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double) getNumerator() / (double) getDenominator();
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      long doubleValue = round(f * d);
      return new Lfraction(doubleValue, d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      String[] objectFraction = s.split("/");
      long a = Long.parseLong(objectFraction[0]);
      long b = Long.parseLong(objectFraction[1]);
      if (b != 0) {
         return new Lfraction(a, b);
      } else {
         throw new RuntimeException("Fraction can't have a 0 as denominator: " + s);
      }
   }

   private static long gcd(long a, long b) {
      long gcd = 1;
      for (int i = 1; i <= abs(a) && i <= abs(b); i++) {
         if (a % i == 0 && b % i == 0) {
            gcd = i;
         }
      }
      return gcd;
   }
}